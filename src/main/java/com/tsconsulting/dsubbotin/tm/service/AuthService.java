package com.tsconsulting.dsubbotin.tm.service;

import com.tsconsulting.dsubbotin.tm.api.repository.IAuthRepository;
import com.tsconsulting.dsubbotin.tm.api.service.IAuthService;
import com.tsconsulting.dsubbotin.tm.api.service.IUserService;
import com.tsconsulting.dsubbotin.tm.enumerated.Role;
import com.tsconsulting.dsubbotin.tm.exception.AbstractException;
import com.tsconsulting.dsubbotin.tm.exception.empty.EmptyIdException;
import com.tsconsulting.dsubbotin.tm.exception.empty.EmptyLoginException;
import com.tsconsulting.dsubbotin.tm.exception.empty.EmptyPasswordException;
import com.tsconsulting.dsubbotin.tm.exception.system.AccessDeniedException;
import com.tsconsulting.dsubbotin.tm.model.User;
import com.tsconsulting.dsubbotin.tm.util.EmptyUtil;
import com.tsconsulting.dsubbotin.tm.util.HashUtil;

public final class AuthService implements IAuthService {

    private final IAuthRepository authRepository;

    private final IUserService userService;

    public AuthService(IAuthRepository authRepository, IUserService userService) {
        this.authRepository = authRepository;
        this.userService = userService;
    }

    @Override
    public String getCurrentUserId() throws AbstractException {
        final String id = authRepository.getCurrentUserId();
        if (EmptyUtil.isEmpty(id)) throw new AccessDeniedException();
        return id;
    }

    @Override
    public void setCurrentUserId(final String id) throws AbstractException {
        if (EmptyUtil.isEmpty(id)) throw new EmptyIdException();
        authRepository.setCurrentUserId(id);
    }

    @Override
    public boolean isAuth() throws AbstractException {
        return getCurrentUserId() != null;
    }

    @Override
    public void login(final String login, final String password) throws AbstractException {
        if (EmptyUtil.isEmpty(login)) throw new EmptyLoginException();
        if (EmptyUtil.isEmpty(password)) throw new EmptyPasswordException();
        final User user = userService.findByLogin(login);
        if (user.isLocked()) throw new AccessDeniedException();
        final String hash = HashUtil.salt(password);
        if (!hash.equals(user.getPasswordHash())) throw new AccessDeniedException();
        setCurrentUserId(user.getId());
    }

    @Override
    public void logout() throws AbstractException {
        final String id = authRepository.getCurrentUserId();
        if (EmptyUtil.isEmpty(id)) throw new AccessDeniedException();
        authRepository.setCurrentUserId(null);
    }

    @Override
    public void registry(final String login,
                         final String password,
                         final String email) throws AbstractException {
        userService.create(login, password, Role.USER, email);
    }

    @Override
    public User getUser() throws AbstractException {
        final String id = authRepository.getCurrentUserId();
        if (EmptyUtil.isEmpty(id)) throw new AccessDeniedException();
        return userService.findById(id);
    }

    @Override
    public void checkRoles(final Role... roles) throws AbstractException {
        if (roles == null || roles.length == 0) return;
        final User user = getUser();
        if (user == null) throw new AccessDeniedException();
        final Role role = user.getRole();
        if (role == null) throw new AccessDeniedException();
        for (final Role item : roles) {
            if (item.equals(role)) return;
        }
        throw new AccessDeniedException();
    }

}
