package com.tsconsulting.dsubbotin.tm.service;

import com.tsconsulting.dsubbotin.tm.api.repository.IRepository;
import com.tsconsulting.dsubbotin.tm.api.service.IService;
import com.tsconsulting.dsubbotin.tm.exception.AbstractException;
import com.tsconsulting.dsubbotin.tm.exception.empty.EmptyIdException;
import com.tsconsulting.dsubbotin.tm.exception.entity.ProjectNotFoundException;
import com.tsconsulting.dsubbotin.tm.exception.system.IndexIncorrectException;
import com.tsconsulting.dsubbotin.tm.model.AbstractEntity;
import com.tsconsulting.dsubbotin.tm.util.EmptyUtil;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public abstract class AbstractService<E extends AbstractEntity> implements IService<E> {

    protected final IRepository<E> repository;

    public AbstractService(IRepository<E> repository) {
        this.repository = repository;
    }

    @Override
    public void add(final E entity) throws AbstractException {
        if (entity == null) throw new ProjectNotFoundException();
        repository.add(entity);
    }

    @Override
    public void remove(final E entity) throws AbstractException {
        if (entity == null) throw new ProjectNotFoundException();
        repository.remove(entity);
    }

    @Override
    public void clear() {
        repository.clear();
    }

    @Override
    public List<E> findAll() {
        return repository.findAll();
    }

    @Override
    public List<E> findAll(final Comparator<E> comparator) {
        if (comparator == null) return Collections.emptyList();
        return repository.findAll(comparator);
    }

    @Override
    public E findById(final String id) throws AbstractException {
        if (EmptyUtil.isEmpty(id)) throw new EmptyIdException();
        return repository.findById(id);
    }

    @Override
    public E findByIndex(final int index) throws AbstractException {
        if (index < 0) throw new IndexIncorrectException();
        return repository.findByIndex(index);
    }

    @Override
    public void removeById(final String id) throws AbstractException {
        if (EmptyUtil.isEmpty(id)) throw new EmptyIdException();
        repository.removeById(id);
    }

    @Override
    public void removeByIndex(final int index) throws AbstractException {
        if (index < 0) throw new IndexIncorrectException();
        repository.removeByIndex(index);
    }

}
