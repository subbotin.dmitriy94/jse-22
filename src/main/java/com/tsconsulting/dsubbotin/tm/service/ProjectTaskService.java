package com.tsconsulting.dsubbotin.tm.service;

import com.tsconsulting.dsubbotin.tm.api.repository.IProjectRepository;
import com.tsconsulting.dsubbotin.tm.api.repository.ITaskRepository;
import com.tsconsulting.dsubbotin.tm.api.service.IProjectTaskService;
import com.tsconsulting.dsubbotin.tm.exception.AbstractException;
import com.tsconsulting.dsubbotin.tm.exception.empty.EmptyIdException;
import com.tsconsulting.dsubbotin.tm.exception.empty.EmptyNameException;
import com.tsconsulting.dsubbotin.tm.exception.entity.ProjectNotFoundException;
import com.tsconsulting.dsubbotin.tm.exception.entity.TaskNotFoundException;
import com.tsconsulting.dsubbotin.tm.exception.system.IndexIncorrectException;
import com.tsconsulting.dsubbotin.tm.model.Project;
import com.tsconsulting.dsubbotin.tm.model.Task;
import com.tsconsulting.dsubbotin.tm.util.EmptyUtil;

import java.util.List;

public final class ProjectTaskService implements IProjectTaskService {

    private final IProjectRepository projectRepository;

    private final ITaskRepository taskRepository;

    public ProjectTaskService(IProjectRepository projectRepository, ITaskRepository taskRepository) {
        this.projectRepository = projectRepository;
        this.taskRepository = taskRepository;
    }

    @Override
    public void bindTaskToProject(
            final String userId,
            final String projectId,
            final String taskId
    ) throws AbstractException {
        isEmpty(userId, projectId, taskId);
        taskRepository.bindTaskToProjectById(userId, projectId, taskId);
    }

    @Override
    public void unbindTaskFromProject(
            final String userId,
            final String projectId,
            final String taskId
    ) throws AbstractException {
        isEmpty(userId, projectId, taskId);
        taskRepository.unbindTaskById(userId, taskId);
    }

    @Override
    public List<Task> findAllTasksByProjectId(
            final String userId,
            final String id
    ) throws AbstractException {
        if (EmptyUtil.isEmpty(id)) throw new EmptyIdException();
        if (!projectRepository.existById(userId, id)) throw new ProjectNotFoundException();
        return taskRepository.findAllByProjectId(userId, id);
    }

    @Override
    public void removeProjectById(final String userId, final String id) throws AbstractException {
        if (EmptyUtil.isEmpty(id)) throw new EmptyIdException();
        if (!projectRepository.existById(userId, id)) throw new ProjectNotFoundException();
        taskRepository.removeAllTaskByProjectId(userId, id);
        projectRepository.removeById(userId, id);
    }

    @Override
    public void removeProjectByIndex(final String userId, final int index) throws AbstractException {
        if (index < 0) throw new IndexIncorrectException();
        final Project project;
        project = projectRepository.findByIndex(userId, index);
        taskRepository.removeAllTaskByProjectId(userId, project.getId());
        projectRepository.removeByIndex(userId, index);
    }

    @Override
    public void removeProjectByName(final String userId, final String name) throws AbstractException {
        if (EmptyUtil.isEmpty(name)) throw new EmptyNameException();
        final Project project;
        project = projectRepository.findByName(userId, name);
        taskRepository.removeAllTaskByProjectId(userId, project.getId());
        projectRepository.removeByName(userId, name);
    }

    private void isEmpty(
            final String userId,
            final String projectId,
            final String taskId
    ) throws AbstractException {
        if (EmptyUtil.isEmpty(projectId)) throw new EmptyIdException();
        if (EmptyUtil.isEmpty(taskId)) throw new EmptyIdException();
        if (!projectRepository.existById(userId, projectId)) throw new ProjectNotFoundException();
        if (!taskRepository.existById(userId, taskId)) throw new TaskNotFoundException();
    }

}
