package com.tsconsulting.dsubbotin.tm.model;

public final class Command {

    private final String name;

    private final String argument;

    private final String description;

    public Command(String name, String argument, String description) {
        this.name = name;
        this.argument = argument;
        this.description = description;
    }

    public String getName() {
        return name;
    }

    public String getArgument() {
        return argument;
    }

    public String getDescription() {
        return description;
    }

    @Override
    public String toString() {
        StringBuilder sbCommandToString = new StringBuilder();
        if (this.name != null && !this.name.isEmpty()) sbCommandToString.append(this.name).append(" ");
        if (this.argument != null && !this.argument.isEmpty())
            sbCommandToString.append(String.format("(%s) ", this.argument));
        if (this.description != null && !this.description.isEmpty())
            sbCommandToString.append("- ").append(this.description);
        return sbCommandToString.toString();
    }

}
