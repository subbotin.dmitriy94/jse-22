package com.tsconsulting.dsubbotin.tm.model;

import java.util.UUID;

public abstract class AbstractOwnerEntity extends AbstractEntity {

    private String userId = UUID.randomUUID().toString();

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    @Override
    public String toString() {
        return super.toString() + " - User Id: " + userId + "; ";
    }

}