package com.tsconsulting.dsubbotin.tm.util;

import java.util.Scanner;

public interface TerminalUtil {

    Scanner SCANNER = new Scanner(System.in);

    static String nextLine() {
        return SCANNER.nextLine();
    }

    static Integer nextNumber() {
        try {
            return Integer.parseInt(SCANNER.nextLine());
        } catch (NumberFormatException e) {
            throw new NumberFormatException("Incorrect number entered!");
        }
    }

    static void printMessage(final String message) {
        System.out.println(message);
    }

}
