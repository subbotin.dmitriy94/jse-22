package com.tsconsulting.dsubbotin.tm.util;

import com.tsconsulting.dsubbotin.tm.enumerated.Role;
import com.tsconsulting.dsubbotin.tm.enumerated.Sort;
import com.tsconsulting.dsubbotin.tm.enumerated.Status;

import java.util.Arrays;

public interface EnumerationUtil {

    static Status parseStatus(final String name) {
        return Arrays
                .stream(Status.values())
                .filter(status -> status.name().equals(name))
                .findFirst().orElse(null);
    }

    static Sort parseSort(final String name) {
        return Arrays
                .stream(Sort.values())
                .filter(sort -> sort.name().equals(name))
                .findFirst().orElse(null);
    }

    static Role parseRole(final String name) {
        return Arrays
                .stream(Role.values())
                .filter(role -> role.name().equals(name))
                .findFirst().orElse(null);
    }

}
