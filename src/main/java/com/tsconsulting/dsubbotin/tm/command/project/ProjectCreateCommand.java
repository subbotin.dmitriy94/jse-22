package com.tsconsulting.dsubbotin.tm.command.project;

import com.tsconsulting.dsubbotin.tm.command.AbstractProjectCommand;
import com.tsconsulting.dsubbotin.tm.enumerated.Role;
import com.tsconsulting.dsubbotin.tm.exception.AbstractException;
import com.tsconsulting.dsubbotin.tm.util.TerminalUtil;

public final class ProjectCreateCommand extends AbstractProjectCommand {

    @Override
    public String name() {
        return "project-create";
    }

    @Override
    public String description() {
        return "Create new project.";
    }

    @Override
    public Role[] roles() {
        return Role.values();
    }

    @Override
    public void execute() throws AbstractException {
        final String currentUserId = serviceLocator.getAuthService().getCurrentUserId();
        TerminalUtil.printMessage("Enter name:");
        final String name = TerminalUtil.nextLine();
        TerminalUtil.printMessage("Enter description:");
        final String description = TerminalUtil.nextLine();
        serviceLocator.getProjectService().create(currentUserId, name, description);
        TerminalUtil.printMessage("[Create]");
    }

}
