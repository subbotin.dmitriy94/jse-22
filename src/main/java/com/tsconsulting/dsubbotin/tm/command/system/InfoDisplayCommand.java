package com.tsconsulting.dsubbotin.tm.command.system;

import com.tsconsulting.dsubbotin.tm.command.AbstractSystemCommand;
import com.tsconsulting.dsubbotin.tm.util.NumberUtil;
import com.tsconsulting.dsubbotin.tm.util.TerminalUtil;

public final class InfoDisplayCommand extends AbstractSystemCommand {

    @Override
    public String name() {
        return "info";
    }

    @Override
    public String arg() {
        return "-i";
    }

    @Override
    public String description() {
        return "Display system info.";
    }

    @Override
    public void execute() {
        final int processors = Runtime.getRuntime().availableProcessors();
        TerminalUtil.printMessage("Available processors: " + processors);
        final long freeMemory = Runtime.getRuntime().freeMemory();
        TerminalUtil.printMessage("Free memory: " + NumberUtil.formatBytes(freeMemory));
        final long maxMemory = Runtime.getRuntime().maxMemory();
        final String maxMemoryFormat = NumberUtil.formatBytes(maxMemory);
        final String maxMemoryValue = maxMemory == Long.MAX_VALUE ? "no limit" : maxMemoryFormat;
        TerminalUtil.printMessage("Maximum memory: " + maxMemoryValue);
        final long totalMemory = Runtime.getRuntime().totalMemory();
        TerminalUtil.printMessage("Total memory available to JVM: " + NumberUtil.formatBytes(totalMemory));
        final long usedMemory = totalMemory - freeMemory;
        TerminalUtil.printMessage("Used memory by JVM: " + NumberUtil.formatBytes(usedMemory));
    }

}
