package com.tsconsulting.dsubbotin.tm.command.system;

import com.tsconsulting.dsubbotin.tm.command.AbstractSystemCommand;
import com.tsconsulting.dsubbotin.tm.util.TerminalUtil;

public final class AboutDisplayCommand extends AbstractSystemCommand {

    @Override
    public String name() {
        return "about";
    }

    @Override
    public String arg() {
        return "-a";
    }

    @Override
    public String description() {
        return "Display developer info.";
    }

    @Override
    public void execute() {
        TerminalUtil.printMessage("Developer: Dmitriy Subbotin");
        TerminalUtil.printMessage("E-Mail: dsubbotin@tsconsulting.com");
    }

}
