package com.tsconsulting.dsubbotin.tm.command.task;

import com.tsconsulting.dsubbotin.tm.command.AbstractTaskCommand;
import com.tsconsulting.dsubbotin.tm.enumerated.Role;
import com.tsconsulting.dsubbotin.tm.enumerated.Sort;
import com.tsconsulting.dsubbotin.tm.exception.AbstractException;
import com.tsconsulting.dsubbotin.tm.model.Task;
import com.tsconsulting.dsubbotin.tm.util.TerminalUtil;

import java.util.Arrays;
import java.util.List;

public final class TaskListShowCommand extends AbstractTaskCommand {

    @Override
    public String name() {
        return "task-list";
    }

    @Override
    public String description() {
        return "Display task list.";
    }

    @Override
    public Role[] roles() {
        return Role.values();
    }

    @Override
    public void execute() throws AbstractException {
        final String currentUserId = serviceLocator.getAuthService().getCurrentUserId();
        TerminalUtil.printMessage("Enter sort:");
        TerminalUtil.printMessage(Arrays.toString(Sort.values()));
        final String sort = TerminalUtil.nextLine();
        List<Task> tasks;
        try {
            Sort sortType = Sort.valueOf(sort);
            TerminalUtil.printMessage(sortType.getDisplayName());
            tasks = serviceLocator.getTaskService().findAll(currentUserId, sortType.getComparator());
        } catch (IllegalArgumentException e) {
            tasks = serviceLocator.getTaskService().findAll(currentUserId);
        }
        int index = 1;
        for (Task task : tasks) TerminalUtil.printMessage(index++ + ". " + task);
    }

}
