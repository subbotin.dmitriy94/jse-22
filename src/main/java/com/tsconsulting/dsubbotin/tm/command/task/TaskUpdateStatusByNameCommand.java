package com.tsconsulting.dsubbotin.tm.command.task;

import com.tsconsulting.dsubbotin.tm.command.AbstractTaskCommand;
import com.tsconsulting.dsubbotin.tm.enumerated.Role;
import com.tsconsulting.dsubbotin.tm.enumerated.Status;
import com.tsconsulting.dsubbotin.tm.exception.AbstractException;
import com.tsconsulting.dsubbotin.tm.exception.system.UnknownStatusException;
import com.tsconsulting.dsubbotin.tm.util.TerminalUtil;

import java.util.Arrays;

public final class TaskUpdateStatusByNameCommand extends AbstractTaskCommand {

    @Override
    public String name() {
        return "task-update-status-by-name";
    }

    @Override
    public String description() {
        return "Update status task by name.";
    }

    @Override
    public Role[] roles() {
        return Role.values();
    }

    @Override
    public void execute() throws AbstractException {
        final String currentUserId = serviceLocator.getAuthService().getCurrentUserId();
        TerminalUtil.printMessage("Enter name:");
        final String name = TerminalUtil.nextLine();
        serviceLocator.getTaskService().findByName(currentUserId, name);
        TerminalUtil.printMessage("Enter status:");
        TerminalUtil.printMessage(Arrays.toString(Status.values()));
        final String statusValue = TerminalUtil.nextLine();
        try {
            final Status status = Status.valueOf(statusValue);
            serviceLocator.getTaskService().updateStatusByName(currentUserId, name, status);
            TerminalUtil.printMessage("[Updated task status]");
        } catch (IllegalArgumentException e) {
            throw new UnknownStatusException();
        }
    }

}
