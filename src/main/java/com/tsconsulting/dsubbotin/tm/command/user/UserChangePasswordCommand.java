package com.tsconsulting.dsubbotin.tm.command.user;

import com.tsconsulting.dsubbotin.tm.command.AbstractUserCommand;
import com.tsconsulting.dsubbotin.tm.enumerated.Role;
import com.tsconsulting.dsubbotin.tm.exception.AbstractException;
import com.tsconsulting.dsubbotin.tm.util.TerminalUtil;

public final class UserChangePasswordCommand extends AbstractUserCommand {

    @Override
    public String name() {
        return "user-change-password";
    }

    @Override
    public String description() {
        return "User change password.";
    }

    @Override
    public Role[] roles() {
        return Role.values();
    }

    @Override
    public void execute() throws AbstractException {
        final String id = serviceLocator.getAuthService().getCurrentUserId();
        TerminalUtil.printMessage("Enter new password:");
        final String newPassword = TerminalUtil.nextLine();
        serviceLocator.getUserService().setPassword(id, newPassword);
        TerminalUtil.printMessage("Password changed.");
    }

}
