package com.tsconsulting.dsubbotin.tm.command.user.admin;

import com.tsconsulting.dsubbotin.tm.command.AbstractUserCommand;
import com.tsconsulting.dsubbotin.tm.enumerated.Role;
import com.tsconsulting.dsubbotin.tm.exception.AbstractException;
import com.tsconsulting.dsubbotin.tm.util.TerminalUtil;

public final class UserLockByLoginCommand extends AbstractUserCommand {

    @Override
    public String name() {
        return "user-lock-by-login";
    }

    @Override
    public String description() {
        return "User lock by login.";
    }

    @Override
    public Role[] roles() {
        return new Role[]{Role.ADMIN};
    }

    @Override
    public void execute() throws AbstractException {
        TerminalUtil.printMessage("Enter login:");
        final String login = TerminalUtil.nextLine();
        serviceLocator.getUserService().lockByLogin(login);
        TerminalUtil.printMessage(String.format("%s user locked.", login));
    }

}
