package com.tsconsulting.dsubbotin.tm.command.task;

import com.tsconsulting.dsubbotin.tm.command.AbstractTaskCommand;
import com.tsconsulting.dsubbotin.tm.enumerated.Role;
import com.tsconsulting.dsubbotin.tm.exception.AbstractException;
import com.tsconsulting.dsubbotin.tm.util.TerminalUtil;

public final class TaskStartByIdCommand extends AbstractTaskCommand {

    @Override
    public String name() {
        return "task-start-by-id";
    }

    @Override
    public String description() {
        return "Start task by id.";
    }

    @Override
    public Role[] roles() {
        return Role.values();
    }

    @Override
    public void execute() throws AbstractException {
        final String currentUserId = serviceLocator.getAuthService().getCurrentUserId();
        TerminalUtil.printMessage("Enter id:");
        final String id = TerminalUtil.nextLine();
        serviceLocator.getTaskService().startById(currentUserId, id);
        TerminalUtil.printMessage("[Updated task status]");
    }

}
