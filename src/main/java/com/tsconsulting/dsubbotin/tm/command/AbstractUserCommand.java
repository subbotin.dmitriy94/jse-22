package com.tsconsulting.dsubbotin.tm.command;

import com.tsconsulting.dsubbotin.tm.exception.AbstractException;
import com.tsconsulting.dsubbotin.tm.exception.entity.UserNotFoundException;
import com.tsconsulting.dsubbotin.tm.model.User;
import com.tsconsulting.dsubbotin.tm.util.TerminalUtil;

public abstract class AbstractUserCommand extends AbstractCommand {

    protected void showUser(final User user) throws AbstractException {
        if (user == null) throw new UserNotFoundException();
        TerminalUtil.printMessage("Id: " + user.getId() + "\n" +
                "Login: " + user.getLogin() + "\n" +
                "First Name: " + user.getFirstName() + "\n" +
                "Last Name: " + user.getLastName() + "\n" +
                "Middle Name: " + user.getMiddleName() + "\n" +
                "E-mail: " + user.getEmail() + "\n" +
                "Role: " + user.getRole().getDisplayName()
        );
    }

}
