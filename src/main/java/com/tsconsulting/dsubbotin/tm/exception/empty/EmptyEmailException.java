package com.tsconsulting.dsubbotin.tm.exception.empty;

import com.tsconsulting.dsubbotin.tm.exception.AbstractException;

public final class EmptyEmailException extends AbstractException {

    public EmptyEmailException() {
        super("Empty email entered.");
    }

}
