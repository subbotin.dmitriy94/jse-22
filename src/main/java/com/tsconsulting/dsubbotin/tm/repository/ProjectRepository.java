package com.tsconsulting.dsubbotin.tm.repository;

import com.tsconsulting.dsubbotin.tm.api.repository.IProjectRepository;
import com.tsconsulting.dsubbotin.tm.enumerated.Status;
import com.tsconsulting.dsubbotin.tm.exception.AbstractException;
import com.tsconsulting.dsubbotin.tm.exception.entity.ProjectNotFoundException;
import com.tsconsulting.dsubbotin.tm.model.Project;

import java.util.Date;
import java.util.Optional;

public final class ProjectRepository extends AbstractOwnerRepository<Project> implements IProjectRepository {

    @Override
    public boolean existById(final String userId, final String id) throws AbstractException {
        return findById(userId, id) != null;
    }

    @Override
    public Project findByName(final String userId, final String name) throws AbstractException {
        return findAll(userId).stream()
                .filter(p -> p.getName().equals(name))
                .findFirst()
                .orElseThrow(ProjectNotFoundException::new);
    }

    @Override
    public void removeByName(final String userId, final String name) throws AbstractException {
        final Optional<Project> optional = Optional.ofNullable(findByName(userId, name));
        optional.ifPresent(this::remove);
    }

    @Override
    public void updateBuyId(
            final String userId,
            final String id,
            final String name,
            final String description
    ) throws AbstractException {
        final Optional<Project> optional = Optional.ofNullable(findById(userId, id));
        optional.ifPresent(p -> {
                    p.setName(name);
                    p.setDescription(description);
                });
    }

    @Override
    public void updateByIndex(
            final String userId,
            final int index,
            final String name,
            final String description
    ) throws AbstractException {
        final Project project = findByIndex(userId, index);
        project.setName(name);
        project.setDescription(description);
    }

    @Override
    public void startById(final String userId, final String id) throws AbstractException {
        final Optional<Project> optional = Optional.ofNullable(findById(userId, id));
        optional.ifPresent(p -> {
            p.setStatus(Status.IN_PROGRESS);
            p.setStartDate(new Date());
        });
    }

    @Override
    public void startByIndex(final String userId, final int index) throws AbstractException {
        final Project project = findByIndex(userId, index);
        project.setStatus(Status.IN_PROGRESS);
        project.setStartDate(new Date());
    }

    @Override
    public void startByName(final String userId, final String name) throws AbstractException {
        final Optional<Project> optional = Optional.ofNullable(findByName(userId, name));
        optional.ifPresent(p -> {
            p.setStatus(Status.IN_PROGRESS);
            p.setStartDate(new Date());
        });
    }

    @Override
    public void finishById(final String userId, final String id) throws AbstractException {
        final Optional<Project> optional = Optional.ofNullable(findById(userId, id));
        optional.ifPresent(p -> p.setStatus(Status.COMPLETED));
    }

    @Override
    public void finishByIndex(final String userId, final int index) throws AbstractException {
        final Project project = findByIndex(userId, index);
        project.setStatus(Status.COMPLETED);
    }

    @Override
    public void finishByName(final String userId, final String name) throws AbstractException {
        final Optional<Project> optional = Optional.ofNullable(findByName(userId, name));
        optional.ifPresent(p -> p.setStatus(Status.COMPLETED));
    }

    @Override
    public void updateStatusById(
            final String userId,
            final String id,
            final Status status
    ) throws AbstractException {
        final Optional<Project> optional = Optional.ofNullable(findById(userId, id));
        optional.ifPresent(p -> {
            p.setStatus(status);
            if (status == Status.IN_PROGRESS) p.setStartDate(new Date());
        });
    }

    @Override
    public void updateStatusByIndex(
            final String userId,
            final int index,
            final Status status
    ) throws AbstractException {
        final Project project = findByIndex(userId, index);
        project.setStatus(status);
        if (status == Status.IN_PROGRESS) project.setStartDate(new Date());
    }

    @Override
    public void updateStatusByName(
            final String userId,
            final String name,
            final Status status
    ) throws AbstractException {
        final Optional<Project> optional = Optional.ofNullable(findByName(userId, name));
        optional.ifPresent(p -> {
            p.setStatus(status);
            if (status == Status.IN_PROGRESS) p.setStartDate(new Date());
        });
    }

}