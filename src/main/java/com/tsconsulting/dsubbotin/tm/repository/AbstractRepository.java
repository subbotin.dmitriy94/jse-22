package com.tsconsulting.dsubbotin.tm.repository;

import com.tsconsulting.dsubbotin.tm.api.repository.IRepository;
import com.tsconsulting.dsubbotin.tm.exception.AbstractException;
import com.tsconsulting.dsubbotin.tm.exception.entity.EntityNotFoundException;
import com.tsconsulting.dsubbotin.tm.exception.system.IndexIncorrectException;
import com.tsconsulting.dsubbotin.tm.model.AbstractEntity;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;

public abstract class AbstractRepository<E extends AbstractEntity> implements IRepository<E> {

    protected final List<E> entities = new ArrayList<>();

    @Override
    public void add(final E entity) {
        entities.add(entity);
    }

    @Override
    public void remove(final E entity) {
        entities.remove(entity);
    }

    @Override
    public void clear() {
        entities.clear();
    }

    @Override
    public List<E> findAll() {
        return entities;
    }

    public List<E> findAll(final Comparator<E> comparator) {
        final List<E> entitiesList = new ArrayList<>(entities);
        entitiesList.sort(comparator);
        return entitiesList;
    }

    @Override
    public E findById(final String id) throws AbstractException {
        return entities.stream()
                .filter(e -> e.getId().equals(id))
                .findFirst()
                .orElseThrow(EntityNotFoundException::new);
    }

    @Override
    public E findByIndex(final int index) throws AbstractException {
        if (entities.size() - 1 < index) throw new IndexIncorrectException();
        return entities.get(index);
    }

    @Override
    public void removeById(final String id) throws AbstractException {
        final Optional<E> optional = Optional.ofNullable(findById(id));
        optional.ifPresent(this::remove);
    }

    @Override
    public void removeByIndex(final int index) throws AbstractException {
        final E entity = findByIndex(index);
        entities.remove(entity);
    }

}