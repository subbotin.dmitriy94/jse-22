package com.tsconsulting.dsubbotin.tm.repository;

import com.tsconsulting.dsubbotin.tm.api.repository.ICommandRepository;
import com.tsconsulting.dsubbotin.tm.command.AbstractCommand;
import com.tsconsulting.dsubbotin.tm.command.AbstractSystemCommand;

import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.Map;

public final class CommandRepository implements ICommandRepository {

    private final Map<String, AbstractSystemCommand> arguments = new LinkedHashMap<>();

    private final Map<String, AbstractCommand> commands = new LinkedHashMap<>();

    @Override
    public AbstractCommand getCommandByName(final String name) {
        return commands.get(name);
    }

    @Override
    public AbstractCommand getCommandByArg(final String arg) {
        return arguments.get(arg);
    }

    @Override
    public Collection<AbstractCommand> getCommands() {
        return commands.values();
    }

    @Override
    public Collection<AbstractSystemCommand> getArguments() {
        return arguments.values();
    }

    @Override
    public void add(final AbstractCommand command) {
        final String name = command.name();
        if (name != null) commands.put(name, command);
        if (command instanceof AbstractSystemCommand) {
            final String arg = ((AbstractSystemCommand) command).arg();
            if (arg != null) arguments.put(arg, ((AbstractSystemCommand) command));
        }
    }

}
