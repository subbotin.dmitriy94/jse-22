package com.tsconsulting.dsubbotin.tm.repository;

import com.tsconsulting.dsubbotin.tm.api.repository.IOwnerRepository;
import com.tsconsulting.dsubbotin.tm.exception.AbstractException;
import com.tsconsulting.dsubbotin.tm.exception.entity.EntityNotFoundException;
import com.tsconsulting.dsubbotin.tm.exception.system.IndexIncorrectException;
import com.tsconsulting.dsubbotin.tm.model.AbstractOwnerEntity;

import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public abstract class AbstractOwnerRepository<E extends AbstractOwnerEntity> extends AbstractRepository<E> implements IOwnerRepository<E> {

    @Override
    public void remove(final String userId, final E entity) {
        final List<E> ownerEntities = findAll(userId);
        ownerEntities.remove(entity);
    }

    @Override
    public void clear(final String userId) {
        final List<E> ownerEntities = findAll(userId);
        ownerEntities.clear();
    }

    @Override
    public List<E> findAll(final String userId) {
        return entities.stream()
                .filter(e -> e.getUserId().equals(userId))
                .collect(Collectors.toList());
    }

    public List<E> findAll(final String userId, final Comparator<E> comparator) {
        return entities.stream()
                .filter(e -> e.getUserId().equals(userId))
                .sorted(comparator)
                .collect(Collectors.toList());
    }

    @Override
    public E findById(final String userId, final String id) throws AbstractException {
        return findAll(userId).stream()
                .filter(e -> e.getId().equals(id))
                .findFirst()
                .orElseThrow(EntityNotFoundException::new);
    }

    @Override
    public E findByIndex(final String userId, final int index) throws AbstractException {
        final List<E> ownerEntities = findAll(userId);
        if (ownerEntities.size() - 1 < index) throw new IndexIncorrectException();
        return ownerEntities.get(index);
    }

    @Override
    public void removeById(final String userId, final String id) throws AbstractException {
        final Optional<E> optional = Optional.ofNullable(findById(userId, id));
        optional.ifPresent(this::remove);
    }

    @Override
    public void removeByIndex(final String userId, final int index) throws AbstractException {
        final List<E> ownerEntities = findAll(userId);
        if (ownerEntities.size() - 1 < index) throw new IndexIncorrectException();
        final E entity = ownerEntities.get(index);
        ownerEntities.remove(entity);
    }

}