package com.tsconsulting.dsubbotin.tm.api.repository;

import com.tsconsulting.dsubbotin.tm.exception.AbstractException;
import com.tsconsulting.dsubbotin.tm.model.AbstractEntity;

import java.util.Comparator;
import java.util.List;

public interface IRepository<E extends AbstractEntity> {

    void add(E entity) throws AbstractException;

    void remove(E entity) throws AbstractException;

    void clear();

    List<E> findAll();

    List<E> findAll(Comparator<E> comparator);

    E findById(String id) throws AbstractException;

    E findByIndex(int index) throws AbstractException;

    void removeById(String id) throws AbstractException;

    void removeByIndex(int index) throws AbstractException;

}