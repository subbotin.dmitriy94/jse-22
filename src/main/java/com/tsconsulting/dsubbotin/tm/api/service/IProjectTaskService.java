package com.tsconsulting.dsubbotin.tm.api.service;

import com.tsconsulting.dsubbotin.tm.exception.AbstractException;
import com.tsconsulting.dsubbotin.tm.model.Task;

import java.util.List;

public interface IProjectTaskService {

    void bindTaskToProject(String userId, String projectId, String taskId) throws AbstractException;

    void unbindTaskFromProject(String userId, String projectId, String taskId) throws AbstractException;

    List<Task> findAllTasksByProjectId(String userId, String id) throws AbstractException;

    void removeProjectById(String userId, String id) throws AbstractException;

    void removeProjectByIndex(String userId, int index) throws AbstractException;

    void removeProjectByName(String userId, String name) throws AbstractException;

}
