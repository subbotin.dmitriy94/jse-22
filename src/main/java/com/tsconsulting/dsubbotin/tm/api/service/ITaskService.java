package com.tsconsulting.dsubbotin.tm.api.service;

import com.tsconsulting.dsubbotin.tm.enumerated.Status;
import com.tsconsulting.dsubbotin.tm.exception.AbstractException;
import com.tsconsulting.dsubbotin.tm.model.Task;

public interface ITaskService extends IOwnerService<Task> {

    void create(String userId, String name) throws AbstractException;

    void create(String userId, String name, String description) throws AbstractException;

    Task findByIndex(String userId, Integer index) throws AbstractException;

    Task findByName(String userId, String name) throws AbstractException;

    void removeByName(String userId, String name) throws AbstractException;

    void updateById(String userId, String id, String name, String description) throws AbstractException;

    void updateByIndex(String userId, int index, String name, String description) throws AbstractException;

    void startById(String userId, String id) throws AbstractException;

    void startByIndex(String userId, int index) throws AbstractException;

    void startByName(String userId, String name) throws AbstractException;

    void finishById(String userId, String id) throws AbstractException;

    void finishByIndex(String userId, int index) throws AbstractException;

    void finishByName(String userId, String name) throws AbstractException;

    void updateStatusById(String userId, String id, Status status) throws AbstractException;

    void updateStatusByIndex(String userId, int index, Status status) throws AbstractException;

    void updateStatusByName(String userId, String name, Status status) throws AbstractException;

}
