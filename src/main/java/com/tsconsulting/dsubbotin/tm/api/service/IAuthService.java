package com.tsconsulting.dsubbotin.tm.api.service;

import com.tsconsulting.dsubbotin.tm.enumerated.Role;
import com.tsconsulting.dsubbotin.tm.exception.AbstractException;
import com.tsconsulting.dsubbotin.tm.model.User;

public interface IAuthService {

    String getCurrentUserId() throws AbstractException;

    void setCurrentUserId(String id) throws AbstractException;

    boolean isAuth() throws AbstractException;

    void logout() throws AbstractException;

    void login(String login, String password) throws AbstractException;

    void registry(String login, String password, String email) throws AbstractException;

    User getUser() throws AbstractException;

    void checkRoles(Role... roles) throws AbstractException;

}
