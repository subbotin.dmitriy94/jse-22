package com.tsconsulting.dsubbotin.tm.api.repository;

import com.tsconsulting.dsubbotin.tm.exception.AbstractException;
import com.tsconsulting.dsubbotin.tm.model.User;

public interface IUserRepository extends IRepository<User> {

    User findByLogin(String login) throws AbstractException;

    User removeByLogin(String login) throws AbstractException;

    boolean isLogin(String login);

}
