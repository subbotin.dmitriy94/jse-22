package com.tsconsulting.dsubbotin.tm.api.repository;

import com.tsconsulting.dsubbotin.tm.exception.AbstractException;
import com.tsconsulting.dsubbotin.tm.model.AbstractOwnerEntity;

import java.util.Comparator;
import java.util.List;

public interface IOwnerRepository<E extends AbstractOwnerEntity> {

    void add(E entity) throws AbstractException;

    void remove(String userId, E entity) throws AbstractException;

    void clear(String userId);

    List<E> findAll(String userId);

    List<E> findAll(String userId, Comparator<E> comparator);

    E findById(String userId, String id) throws AbstractException;

    E findByIndex(String userId, int index) throws AbstractException;

    void removeById(String userId, String id) throws AbstractException;

    void removeByIndex(String userId, int index) throws AbstractException;

}